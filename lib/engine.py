from os import system
from random import randint

class Engine:
    def __init__(self):
        self.players = {}
        self.log = {}
        self.snake_or_ladder = [(2, 38), (16, 6), (7, 14), (8, 31), 
                                (49, 11), (15, 26), (62, 19), (21, 42), 
                                (46, 25), (28, 84), (36, 44), (51, 67),
                                (74, 53), (64, 60), (89, 68), (71, 91),
                                (95, 75), (78, 98), (99, 80), (92, 88)]

    def clrscr(self):
        system("cls")

    def game_start(self):
        self.clrscr()
        ready = False

        while not ready:
            player_count = input("Zadejte počet hráčů > ")
            self.clrscr()

            try:
                player_count = int(player_count)
            except ValueError:
                input("Nezadali jste číslo. Prosím zadejte znova (enter).")
                self.clrscr()
            else:
                if player_count > 1:
                    ready = True
                    name_count = 1
                    while name_count != player_count + 1:
                        new_player_name = input(f"Zadejte jméno {name_count}. hráče > ")
                        self.clrscr()
                        try:
                            self.players[new_player_name]
                        except KeyError:
                            self.players[new_player_name] = 1
                            name_count += 1
                        else:
                            input(f"Hráč se jménem \"{new_player_name}\" již existuje. Prosím zadejte jiné jméno (enter).")
                            self.clrscr()
                else:
                    input("Počet hráů musí být větší než 1. Prosím zadejte správný počet hráčů (enter).")
                    self.clrscr()

        for i, name in enumerate(self.players):
            print(f"{i + 1}. hráč\t{name}")
            self.log[name] = [0]

        input("\n---\n\nHra je připravena (enter pro start).")

    def game(self):
        self.clrscr()
        game_ends = False

        while not game_ends:
            for i, name in enumerate(self.players):
                input(f"Hod {i + 1}. hráče {name} (enter).")
                roll = self.roll()
                print(f"Hráč {name} hodil {roll}.")

                if self.players[name] + roll > 100:
                    print(f"Hráč {name} přehodil pole 100. Jeho tah je vynechán.")
                else:
                    self.players[name] += roll
                    print(f"Hráč {name} se posunul na pole {self.players[name]}.")
                    self.check_field(name)

                self.log[name] += [self.players[name]]

                print("\n--- Aktuální stav ---\n")
                for i, name in enumerate(self.players):
                    print(f"{i + 1}. hráč\t{name}{' ' * (20 - len(name))}{self.players[name]}")
                    if self.players[name] == 100:
                        game_ends = True
                        
                input("\n---\n\nEnter pro pokračování.")
                if game_ends:
                    break
                self.clrscr()            

    @staticmethod
    def roll():
        return randint(1, 6)

    def check_field(self, player_name):
        for event in self.snake_or_ladder:
            if self.players[player_name] == event[0]:
                self.players[player_name] = event[1]
                event_name = "hadovi"
                if event[0] < event[1]:
                    event_name = "žebříku"
                print(f"Hráč {player_name} se posunul díky {event_name} z pole {event[0]} na pole {event[1]}.")
                break
        self.player_on_field(player_name)
        
    def player_on_field(self, player_name):
        for name in self.players:
            if self.players[name] == self.players[player_name] and name != player_name:
                self.players[name] -= 1
                print(f"Hráč {player_name} posunul hráče {name} z pole {self.players[player_name]} na pole {self.players[name]}.")
                self.check_field(name)
                break

    def game_end(self):
        self.clrscr()
        winner = None
        for i, name in enumerate(self.players):
            print(f"{i + 1}. hráč\t{name}{' ' * (20 - len(name))}{self.players[name]}")
            if self.players[name] == 100:
                        winner = name
        show_log = input(f"\n---\n\nVyhrává hráč {winner}. Ukázat průběh hry? (vstup pro ano) > ")
        if show_log:
            self.clrscr()
            players = []
            for player in self.players:
                players.append(player)
                print(f"{player}\t\t", end="")
            print("")
            for i in range(len(self.log[players[0]])):
                for player in players:
                    status = 0
                    try:
                        print(f"{self.log[player][i]}\t\t", end="")
                    except IndexError:
                        print(f"-\t\t", end="")
                print("")
        input("")
